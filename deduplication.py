from __future__ import print_function
import struct
import collections
import random

window_size = 4  # less than 10=(4+4+1)=(int+int+escape) does not make sense
fingerprint_map = collections.OrderedDict()
library_position = 0
escape_char = {1: struct.pack('B', 120)}

fingerprint_map_max_size = 1000
nr_of_least_common_escape_chars = 10
input_file = "a"
input_file_fd = open(input_file, "rb")
file_data = input_file_fd.read()
input_file_fd.close()
file_data_len = len(file_data)
output_file = "a.dedup"
output_file_fd = open(output_file, "w")
matching = False
match_end = 0
match_length = 0
library_match_start = 0
window_match_end = 0
escape_counter = collections.Counter()
window_tail = 0
window = file_data[:(window_size-1)]
# Initialize escape char counter
for i in range(0, 255):
    escape_counter[struct.pack('B', i)] = 0


def select_least_common_escape_char():
    least_commons = escape_counter.most_common()[:-nr_of_least_common_escape_chars:-1]
    return random.choice(least_commons)[0]


def write_plain(data):
    output_file_fd.write(data)
    print(data, end='')   # for debugging


def write_reference(start, length):
    output_file_fd.write(escape_char[1] + struct.pack('i', start) + struct.pack('i', length))
    print("(" + str(start) + "," + str(length) + ")", end='')  # for debugging


# If we encounter an escape char in our data,
# we prevent any problems by immeadiately changing
# the escape character by writing a change_escape_char-token
# to the data in reference-form, like:
# (start,length) = (new_escape_char,0)
# to make data-bloating a rare event, we select one of the
# characters we have seen the rarest at random.
# Note thtat the escape char is just 8bit => convert to 32bit
def write_change_escape():
    least_common_esc_char = select_least_common_escape_char()
    write_reference((0 << 32) + int(struct.unpack('B', least_common_esc_char)[0]), 0)
    escape_char[1] = least_common_esc_char


# Count first window chars for escape-counter
# and set escape sequence (to the least common so far),
# then write out escape-sequence to tell receiver the used escape sequence
for i in range(0, file_data_len if file_data_len < (window_size-1) else (window_size-1)):
    escape_counter[file_data[i]] += 1
least_common_escape_char = select_least_common_escape_char()
escape_char[1] = least_common_escape_char
write_plain(escape_char[1])

if file_data_len < window_size:
    write_plain(file_data)
    exit(0)

i = window_head = window_size - 1  # head/tail works like:    ...->....*tail*.....*head*...->...

while i <= file_data_len:
    if i == file_data_len:
        if matching:
            write_reference(library_match_start, match_length)
        else:
            if window_tail < window_match_end:
                write_plain(file_data[window_match_end:])
            else:
                write_plain(file_data[-(window_size-1):])
        break

    c = file_data[i]
    window += c
    window = window[-window_size:]

    escape_counter[c] += 1

    if c == escape_char[1]:
        write_change_escape()

    if matching:
        if c == file_data[library_match_start + match_length]:
            match_length += 1
        else:
            matching = False
            write_reference(library_match_start, match_length)
            window_match_end = window_head
            match_length = 0
    else:
        if window in fingerprint_map and window_tail >= window_match_end:
            matching = True
            match_length = window_size
            match_start = window_tail
            library_match_start = fingerprint_map[window]

    if not matching and window_tail >= window_match_end:
        write_plain(file_data[window_tail])

    fingerprint_map[window] = window_tail
    i += 1

    window_head = i
    window_tail = window_head - (window_size - 1)

    if len(fingerprint_map) >= fingerprint_map_max_size:
        if matching:
            matching = False
            write_reference(library_match_start, match_length)
            window_match_end = window_head
            match_length = 0
        fingerprint_map.popitem(last=False)

output_file_fd.close()

# todo: multiple window hashes in map
# todo: 8bit vs 32/64bit chars (evtl alles auf 32bit umstellen?)
