from __future__ import print_function
import struct
cmp_file = "a"  # just for testing, remove this later
cmp_file_fd = open(cmp_file, "rb")
input_file = "a.dedup"
input_file_fd = open(input_file, "rb")
output_file = "a.redup"
output_file_fd = open(output_file, "w")
output_file_data = ""
output_file_data_max_len = 1000
i = 0


def write_output(data):
    global output_file_data

    output_file_data += data
    if len(output_file_data) >= output_file_data_max_len:
        output_file_data = output_file_data[1:]

    output_file_fd.write(data)

    # compare with original for debugging
    print(data, end='')
    data_length = len(data)
    redup_out = data
    original_data = cmp_file_fd.read(data_length)
    if redup_out != original_data:
        print("Error cmp failed")
        exit(1)

current_char = input_file_fd.read(1)

# Read in escape char
escape_seq = current_char

current_char = input_file_fd.read(1)
while current_char != "":
    if current_char == escape_seq:
        position = struct.unpack('i', input_file_fd.read(4))[0]
        length = struct.unpack('i', input_file_fd.read(4))[0]
        if length == 0:
            # Change escape char
            escape_seq = position  # todo: has to be fixed with struct
        else:
            for current_char_pos in range(position, position+length):
                current_char = output_file_data[current_char_pos]
                write_output(current_char)
    else:
        write_output(current_char)

    current_char = input_file_fd.read(1)

input_file_fd.close()
output_file_fd.close()
cmp_file_fd.close()
